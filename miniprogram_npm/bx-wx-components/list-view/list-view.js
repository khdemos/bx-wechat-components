Component({
   // 启用插槽
   options: {
    multipleSlots: true
  },
  /**
   * 组件的属性列表
   */
  properties: {
    // 开启刷新功能
    useRefresh: {
      type: Boolean,
      value: true,
    },
    // 开启加载更多功能
    useLoadMore: {
      type: Boolean,
      value: true,
    },
    // 数据
    datas: {
      type: Array,
      value: [],
      observer: 'onDatasChange'
    },
    // limt 分页一次请求数据量，默认为10
    limit: {
      type: Number,
      value: 10,
    },
    // 如果到底不触发加载更多，throttle设置为false, 开启不限制scrollview节流，尽量不要设置，可能有性能问题
    throttle: {
      type: Boolean,
      value: true,
    },
    // 加载失败
    error: {
      type: Boolean,
      value: false,
      observer: 'onError'
    },
    // list-view 初始化正在加载组件，默认为false，如果page页面没有初始化加载loading，可以开启此属性，当列表数据初次加载完毕后，需要关闭此属性
    loading: {
      type: Boolean,
      value: false,
    },
    // 是否适配iphone x
    adapterIPhoneX: {
      type: Boolean,
      value: true,
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    refreshState: 0, // 下拉刷新状态 0：无状态 1: 下拉中 2：下拉达到刷新条件 3：下拉刷新中 4：下拉失败
    loadMoreState: 0, // 上拉加载状态 0：无状态 1：上拉加载中 2: 没有更多数据 3：加载失败
  },

  /**
   * 组件的方法列表
   */
  methods: {
    /**
     * 监听数据变化
     */
    onDatasChange(newVal = [], oldVal = []) {
      // 判断是否没有更多数据
      if (newVal.length > 0 && newVal.length >= oldVal.length) {
        if (newVal.length - oldVal.length < this.data.limit) {
          // 没有更多数据
          this.setData({
            loadMoreState: 2,
          })
        } else {
          this.resetLoadMoreState()
        }
      }
      // 下拉完成，重置下拉状态
      if (this.data.refreshState === 3 && !this.data.error) {
        this.resetRefreshState()
        this.resetLoadMoreState()
        if(newVal.length > 0 && newVal.length < this.data.limit) {
          this.setData({
            loadMoreState: 2,
          })
        }
      }
    },

    /**
     * 网络异常监听
     */
    onError(newVal, oldVal) {
      // 数据异常
      if (oldVal === false && newVal === true) {
        if (this.data.refreshState === 3) {
          this.setData({
            refreshState: 4,
          })
          const timmer = setTimeout(() => {
            this.resetRefreshState()
            clearTimeout(timmer)
          }, 1000)
        } else if (this.data.loadMoreState === 1) {
          this.setData({
            loadMoreState: 3,
          })
        }
      }
    },
  
    /**
     * 滚动到底部
     */
    scrollBottomListener(e) {
      //加载更多
      if (this.data.refreshState !== 3 &&
        this.data.datas.length > 0 &&
        this.data.useLoadMore &&
        this.data.loadMoreState !== 1 && 
        this.data.loadMoreState !== 2) {
          this.setData({
            loadMoreState: 1,
            error: false,
          })
          this.triggerEvent("onLoadMore")
      }
    },
  
    /**
     * 重置加载更多状态
     */
    resetLoadMoreState() {
      this.setData({
        loadMoreState: 0,
      })
    },
  
    /**
     * 重置下拉状态
     */
    resetRefreshState() {
      this.setData({
        refreshState: 0,
      })
    },

    /**
     * 上拉加载错误点击事件
     */
    onErrorRefresh() {
      this.setData({
        loadMoreState: 1,
        error: false,
      })
      this.triggerEvent("onLoadMore")
    },

    /**
     * 下拉刷新
     */
    onRefresh() {
      this.setData({
        refreshState: 3,
      })
      this.triggerEvent("onRefresh")
    },
  }
})