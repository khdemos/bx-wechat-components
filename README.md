# list-view 组件

![下拉刷新](https://gitee.com/khdemos/bx-wechat-components/raw/master/img1.jpg)
![上拉加载](https://gitee.com/khdemos/bx-wechat-components/raw/master/img2.jpg)

## properties

|  属性名称     | 默认值   |  必须   | 描述                 |
|  :--------:  | :-------:| :----:  | :-------------:     |
| datas        | []       |true     |列表数据          |
| error       | false   |true    |数据是否发生错误（网络异常）|
| useRefresh   | true     |false    |是否开启下拉刷新          |
| useLoadMore  | true      |false    |是否开启加载更多         |
| limit        | 10       |false    |分页： 每页条数           |
| throttle     | true     |false    |如果到底不触发加载更多，throttle设置为false, 开启不限制scrollview节流，尽量不要设置，可能有性能问题           |
| loading     | false     |false    |list-view 初始化正在加载状态组件，默认为false，如果page页面没有初始化加载loading，可以开启此属性，当列表数据初次加载完毕后，需要关闭此属性          |
| adapterIPhoneX     | true     |false    | 适配iphonex         |

## 事件

|  事件名称     | 描述                 |
|  :--------:  | :-------------:     |
| onRefresh    | 下拉刷新事件          |
| onLoadMore    | 加载更多事件          |

## 代码示例

* json文件引入

```
"list-view": "bx-wx-components/list-view/list-view"
```

* js文件

```
import {api} from '../../../config/index'
import {request} from '../../../common/index'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    carousels: [],
    loading: true,
    error: false,
    recommendDatas: [],
  },
  page: 1,
  pageCount: 10,
  onLoad() {
    this._getCarousel()
    this._getRecommend(1)
  },

  /**
   * 获取推荐列表
   */
  _getRecommend(page) {
    // 请求数据
    request.post({
      url: api.recommend_list,
      params:{
        per_page: this.pageCount,
        page: page,
      },
      useToken: false,
      tag:'获取推荐列表'
    }).then(res => {
      this.page = page
      this.setData({
        recommendDatas: page===1?res.data:[...this.data.recommendDatas, ...res.data],
        loading: false,
        error: false,
      })
      this.page++
    }).catch(e => {
      this.setData({
        loading:false,
        error: true,
      })  
    })
  },

  /**
   * 获取轮播图
   */
  _getCarousel() {
    // 请求数据
    request.post({
      url: api.carousel_get_list,
      params:{},
      useToken: false,
      tag:'获取轮播图'
    }).then(res => {
      this.setData({
        carousels: res.data
      })
    }).catch(e => {
      wx.showToast({
        title: '网络异常',
        icon: 'none',
      }) 
    })
  },

  onRefresh() {
    this.setData({
      loading:true,
      error: false,
    })
    this._getRecommend(1)
    this._getCarousel()
  },

  onLoadMore() {
    this._getRecommend(this.page)
  },

})

```
* wxml
```
<view class="container">
  <list-view adapterIPhoneX="{{false}}" loading="{{loading}}" datas='{{recommendDatas}}' error="{{error}}" bind:onRefresh='onRefresh' bind:onLoadMore='onLoadMore'>
    <view class="header" slot="header">
      <home-swiper datas="{{carousels}}" />
      <projects />
      <rural-map />
      <van-divider
        contentPosition="center"
        customStyle="color: #505050; border-color: #bdbdbd;font-size: 18px; font-weight: bold;padding: 0 200rpx;"
      >
        推荐
      </van-divider>
    </view>
    <block wx:for="{{recommendDatas}}" wx:key="index">
      <ticket-item useSubscript wx:if="{{item.p_type === '5'}}" item="{{item}}" />
      <rural-item useSubscript wx:elif="{{item.p_type === '3'}}" item="{{item}}" />
      <domestic-item useSubscript wx:elif="{{item.p_type === '1'}}" item="{{item}}" />
      <international-item useSubscript wx:elif="{{item.p_type === '2'}}" item="{{item}}" />
      <life-item useSubscript wx:elif="{{item.p_type === '4'}}" item="{{item}}" />
    </block>
  </list-view>
</view>
```

* wxss

```
.container {
  background-color: #f2f2f2;
  height: 100vh;
  display: flex;
  flex-direction: column;
}

.header {
  display: flex;
  flex-direction: column;
}

```